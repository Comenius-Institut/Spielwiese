# Die Passions- und Ostergeschichte nach Matthäus, erzählt mit Liedern unserer Zeit

## Jesus zieht in Jerusalem ein (Matthäus 21, 1-11)

- [rockstar - Post Malone](https://www.youtube.com/watch?v=UceaB4D0jpo)
- [willkommen zurück - Mark Forster](https://www.youtube.com/watch?v=RikfQteDxSs)
- [good life - Kehlani](https://www.youtube.com/watch?v=jZhQOvvV45w)
- [welcome home son - Rachial Fade](https://www.youtube.com/results?search_query=welcome+home+son+Rachial+Fade)
- [globus-preliator - Lisbeth Scott](https://www.youtube.com/results?search_query=globus-preliator+Lisbeth+Scott)

## Jesus feiert mit seinen Jüngern ein letztes Mal (Matthäus 26, 1-11)

- [Bye, bye, bye - NSYNC](https://www.youtube.com/watch?v=Eo-KmOd3i7s)
- [wenn einfach so einfach wär - SDP](https://www.youtube.com/watch?v=Wx2FDyvt0gI)
- [au revoir - Mark Forster](https://www.youtube.com/watch?v=7Y6_w5Z9WbU)

## Jesus betet im Garten Gethsemane (Matthäus 26, 36-46)

- [thank god - Travis Scott](https://www.youtube.com/results?search_query=thank+god+travis+scott)
- [god's plan - Drake](https://www.youtube.com/watch?v=xpVfcZ0ZcFM)
- [pray for me - The Weeknd](https://www.youtube.com/watch?v=XR7Ev14vUh8)

## Jesus wird verhaftet und vor Gericht gestellt (Matthäus 26, 47-75)

- [Knastbrief - Kontra K](https://www.youtube.com/watch?v=8mV3xNjGs3U)
- [end of beginning - Djo](https://www.youtube.com/results?search_query=end+of+beginning+Djo)
- [sound of da police - KRS-One](https://www.youtube.com/watch?v=9ZrAYxWPN6c)

## Jesus wird zum Tod verurteilt (Matthäus 27, 11-31)

- [thank god - Travis Scott](https://www.youtube.com/results?search_query=thank+god+travis+scott)
- [no time to die - Billie Eilish](https://www.youtube.com/watch?v=GB_S2qFh5lU)
- [you found me - The Fray](https://www.youtube.com/watch?v=jFg_8u87zT0)
- [Anhörung - KC Rebell](https://www.youtube.com/watch?v=aBs5bS-Mr2I)

## Jesus stirbt am Kreuz (Matthäus 27, 32-56)

- [die for you (with Ariana) Remix](https://www.youtube.com/results?search_query=die+for+you+ariana+remix)
- [ruhe in Frieden - Nici Haye](https://www.youtube.com/results?search_query=ruhe+in+Frieden+Nici+Haye)
- [last november - Machine Gun Kelly](https://www.youtube.com/watch?v=o9n9_vyJ2HU)

## Jesus wird begraben (Matthäus 27, 57-66)

- [ne Leiche - SDP feat. SIDO](https://www.youtube.com/watch?v=Kj4ICGk9tLc)
- [bury a friend - Billie Eilish](https://www.youtube.com/watch?v=HUHC9tYz8ik)
- [if i die young - The Band Perry](https://www.youtube.com/watch?v=7NJqUN9TClM)
- [lebendig begraben - Dame](https://www.youtube.com/watch?v=LEz8ZLFfaW0)
- [von guten Mächten - Wonderful Powerful Forces](https://www.youtube.com/results?search_query=von+guten+Mächten)

## Jesus lebt (Matthäus 28, 1-10)

- [er ist erstanden, halleluja - Osterlied](https://www.youtube.com/results?search_query=er+ist+erstanden+halleluja)
- [Viva La Vida - Coldplay](https://www.youtube.com/watch?v=dvgZkm1xWPE)
- [I love it - Icona Pop](https://www.youtube.com/watch?v=UxxajLWwzqY)

## Jesus beauftragt seine Jünger (Matthäus 28, 16-20)

- [Hall of fame - The Script feat. will.i.am](https://www.youtube.com/watch?v=mk48xRzuNvA)
- [Psalm 135 - Byzantine](https://www.youtube.com/watch?v=xVvGkcScfs0)
- [open arms - SZA](https://www.youtube.com/watch?v=55AP-Q7_aPo)
